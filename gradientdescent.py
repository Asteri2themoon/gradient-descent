import matplotlib.pyplot as plt
import sympy as sp
import numpy as np
import random

"""
GradientDescent:
classe pour trouver les paramètres qui permettent a une fonction de se rapprocher le plus possible d'un ensemble de points, la classe cherche a minimiser le carre de l'erreur entre les points et la courbe
La classe a uniquement besoin de l'expression de la fonction (formée avec sympy), de la variable de cette fonction (symbole de sympa utilise dans la fonction) et de la liste des différents paramètres à faire varier pour s'approcher des points.
Une fois initialiser, on doit fixer la "learning rate" avec la fonction setLearningRate(leanrningRate), la learning rate définit la vitesse d'apprentissage, la valeur doit être assez petite pour que les paramètres convergent mais aussi assez grande pour que la vitesse à laquelle les paramètres convergent soit suffisante.
On doit ensuite effectuer la descente avec la fonction iteration(X, Y) jusqu'à ce que les paramètres aient convergé, les variables X et Y sont des tableaux qui contiennent les abscisses et ordonnées des points
La fonction getError(X, Y) permet d'évaluer la performance des paramètres actuelle
Ne jamais utiliser les symboles "_ysample_" et "_xsample_" dans une expression avec la classe GradientDescent

L'exemple ci-dessous montre comment utiliser la classe GradientDescent avec une fonction affine mais la classe permet de le faire avec toute fonction définie de R dans R
"""
class GradientDescent:
	function=None#fonction à apprendre (fonction définie de R dans R)
	variable=None#variable de l'espace d'entrer de la fonction
	parameter=[]#parameter à ajuster pour se rapprocher de la courbe
	values=[]#valeurs actuelle des paramètres
	gradient=[]#expression du gradient de l'erreur (sans la somme)
	learningRate=0.001
	
	#calcule l'expression du gradient de l'erreur (sans la somme)
	def calculateGradient(self,f,var,param):
		y=sp.Symbol('_ySample_')
		x=sp.Symbol('_xSample_')
		E=(y-f.subs(t,x))**2
		grad=[]
		for p in param:
			grad.append(sp.diff(E,p))
		return grad
	
	def setFunction(self,f,var,par):
		self.function=f
		self.variable=var
		self.parameter=par
		self.values=[]
		for i in range(len(self.parameter)):
			self.values.append(random.random()/100)
		self.gradient=self.calculateGradient(self.function,self.variable,self.parameter)
	
	def setLearningRate(self,lr):
		self.learningRate=lr
	
	def getError(self,X,Y):
		E=0
		v=[]
		for i in range(len(self.parameter)):
			v.append((self.parameter[i],self.values[i]))
		for i in range(len(X)):
			p=[(self.variable,X[i])]
			p.extend(v)
			E+=(Y[i]-self.function.subs(p))**2
		return E/len(X)
	
	def getParameter(self):
		param=[]
		for i in range(len(self.parameter)):
			param.append((self.parameter[i],self.values[i]))
		return param
	
	def iteration(self,X,Y):
		y=sp.Symbol('_ySample_')
		x=sp.Symbol('_xSample_')
		n=len(X)
		currentGrad=[]
		v=[]
		for i in range(len(self.parameter)):
			v.append((self.parameter[i],self.values[i]))
		for g in self.gradient:
			sum=0
			for i in range(n):
				p=[(x,X[i]),(y,Y[i])]
				p.extend(v)
				sum+=float(g.subs(p))/n
			currentGrad.append(sum)
		for i in range(len(self.parameter)):
			self.values[i]=self.values[i]-currentGrad[i]*self.learningRate

	
	def generatePoints(self,lst):
		y=[]
		configParam=[]
		for i in range(len(self.parameter)):
			configParam.append((self.parameter[i],self.values[i]))
		for x in lst:
			v=[(self.variable,x)]
			v.extend(configParam)
			y.append(float(f.subs(v)))
		return y
	
	def __init__(self,f,var,par):
		self.setFunction(f,var,par)

sp.init_printing()
X=[2,5,7,9,0]#points
Y=[4,6,10,9,0]
t=sp.Symbol('t')#variable de la fonction
a=sp.Symbol('a')#paramètres de la fonction
b=sp.Symbol('b')
f=a*t+b#définition de la fonction

test=GradientDescent(f,t,[a,b])#initialisation
test.setLearningRate(0.01)#définition de la learning rate
print("random parameter, error: {0}".format(test.getError(X,Y)))
print("iteration:")
for i in range(10):#on peut utiliser la fonction getError pour trouver un "critère de convergence" et arrêter la boucle plutôt que d'utiliser une boucle fort
	test.iteration(X,Y)#itération de la descente de gradient
	print("iteration {0}, error: {1}".format(i+1,test.getError(X,Y)))
print("final error:")
print(test.getError(X,Y))#erreur final de la fonction
print("final parameter:")
print(test.getParameter())#paramètre trouvé par la descente de gradient

xSamples=np.arange(0,10,0.1)#générer une liste de points avec les nouveaux paramètres
ySamples=test.generatePoints(xSamples)

plt.plot(X,Y,'ro')#affichage des points et de la courbe trouvée
plt.plot(xSamples,ySamples,'g')
plt.margins(0.1,0.1)
plt.show()