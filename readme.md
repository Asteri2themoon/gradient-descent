# Descente de gradient

### Description
Implémentation simple de l'algorithme de descente de gradient pour optimiser les paramètres de toutes fonctions allant de R dans R peu import ça forme grâce au calcul formel avec sympy.

![alt tag](http://imgur.com/download/MQ5FuB3)
(exemple avec une fonction affine)

### Language
python
### Dépendance
  - sympy
  - numpy
  - mathplotlib (uniquement pour l'affichage)

### License
MIT